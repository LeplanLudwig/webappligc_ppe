package dto.region;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
// Ne pas oublier sur toutes les entités XML et résumés
public class ResumeRegion {
    
  private String    codeRegion;
  private String    nomRegion;
  private Float     caAnnuel;
  private int      nbClient;

  public ResumeRegion(){}
  
  public ResumeRegion(String codeRegion,String nomRegion, Float caAnnuel, int nbClient){
       
      
        this.codeRegion = codeRegion;
        this.nomRegion = nomRegion;
        this.caAnnuel = caAnnuel;
        this.nbClient = nbClient;

  }

    public int getNbClient() {
        return nbClient;
    }

    public void setNbClient(int nbClient) {
        this.nbClient = nbClient;
    }

    public Float getCaAnnuel() {
        return caAnnuel;
    }

    public void setCaAnnuel(Float caAnnuel) {
        this.caAnnuel = caAnnuel;
    }

    public String getCodeRegion() {
        return codeRegion;
    }

    public void setCodeRegion(String codeRegion) {
        this.codeRegion = codeRegion;
    }

    public String getNomRegion() {
        return nomRegion;
    }

    public void setNomRegion(String nomRegion) {
        this.nomRegion = nomRegion;
    }



}
    //<editor-fold defaultstate="collapsed" desc="getters et setters">

