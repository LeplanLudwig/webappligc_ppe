/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package dto.lignedecommande;

import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author remy917
 */
@XmlRootElement
public class ResumeLigneDeCommande {
    private String refprod;
    private String desigProd;
    private Float prixProd;
    private Float qteCom;
    private Float montantHT;
    private Float montantTTC;
    
    public ResumeLigneDeCommande(){
        
    }
    public ResumeLigneDeCommande(String refprod, String desigProd, Float prixProd, Float qteCom, Float montantHT, Float montantTTC){
        
      this.desigProd = desigProd;
      this.montantHT = montantHT;
      this.montantTTC = montantTTC;
      this.prixProd = prixProd;
      this.qteCom = qteCom;
      this.refprod = refprod;
      
    }
      

    public String getRefprod() {
        return refprod;
    }

    public void setRefprod(String refprod) {
        this.refprod = refprod;
    }

    public String getDesigProd() {
        return desigProd;
    }

    public void setDesigProd(String desigProd) {
        this.desigProd = desigProd;
    }

    public Float getPrixProd() {
        return prixProd;
    }

    public void setPrixProd(Float prixProd) {
        this.prixProd = prixProd;
    }

    public Float getQteCom() {
        return qteCom;
    }

    public void setQteCom(Float qteCom) {
        this.qteCom = qteCom;
    }

    public Float getMontantHT() {
        return montantHT;
    }

    public void setMontantHT(Float montantHT) {
        this.montantHT = montantHT;
    }

    public Float getMontantTTC() {
        return montantTTC;
    }

    public void setMontantTTC(Float montantTTC) {
        this.montantTTC = montantTTC;
    }
}
