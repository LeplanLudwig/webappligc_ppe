
package work.controleurs;

import bal.categorieproduit.BalCategorieProduit;
import dao.categorieproduit.DaoCategorieProduit;
import entites.CategorieProduit;
import java.io.Serializable;
import javax.annotation.PostConstruct;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;
import org.primefaces.model.chart.PieChartModel;

/**
 *
 * @author rsmon
 */

@Named
@RequestScoped
public class ContCamembert implements Serializable {
    
   
  @Inject private DaoCategorieProduit daoCategorieProduit;
  
  @Inject private BalCategorieProduit balCategorieProduit;
    
  private PieChartModel camembert;
  
  @PostConstruct
  public void init(){
    
    camembert=new PieChartModel();
   
    for (CategorieProduit cp: daoCategorieProduit.getToutesLesCategories()){
       
        camembert.set(cp.getNomCateg(), balCategorieProduit.caCategorieProduit(cp));    
    }
  }
  
  //<editor-fold defaultstate="collapsed" desc="GETTERS ET SETTERS">
  
   public PieChartModel getCammebert() {
        return camembert;
    }
   
  //</editor-fold>
}
