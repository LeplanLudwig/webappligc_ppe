package work.controleurs;

import bal.commande.BalCommande;
import dao.commande.DaoCommande;
import entites.Commande;
import java.io.Serializable;
import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;

/**
 *
 * @author rsmon
 */

@Named
@SessionScoped
public class ContJsfCommande implements Serializable{
  
    @Inject
    private DaoCommande daoCommande;
    
    @Inject BalCommande balCommande;
  
    private Long   numcomRecherche;
    private Commande commande;
    
    private Float  caAnnuelCommande;
    
    public void ecouteurRecherche(){
    
        if(numcomRecherche!=null){ 
            
            commande=daoCommande.getCommande(numcomRecherche);
            
            if (commande!=null){
                caAnnuelCommande=balCommande.montantCommandeTTC(commande);
            }
            else {
             
             commande=new Commande(); caAnnuelCommande=null;

             FacesContext contexte=FacesContext.getCurrentInstance();
             contexte.addMessage(null,
                                 new FacesMessage(FacesMessage.SEVERITY_INFO, 
                                 null, "PAS COMMANDE CONNU AVEC CE NUMERO."));  
           }
            
        }
    }

    public DaoCommande getDaoCommande() {
        return daoCommande;
    }

    public void setDaoCommande(DaoCommande daoCommande) {
        this.daoCommande = daoCommande;
    }

    public BalCommande getBalCommande() {
        return balCommande;
    }

    public void setBalCommande(BalCommande balCommande) {
        this.balCommande = balCommande;
    }

    public Long getNumcomRecherche() {
        return numcomRecherche;
    }

    public void setNumcomRecherche(Long numcomRecherche) {
        this.numcomRecherche = numcomRecherche;
    }

    public Commande getCommande() {
        return commande;
    }

    public void setCommande(Commande commande) {
        this.commande = commande;
    }

    public Float getCaAnnuelCommande() {
        return caAnnuelCommande;
    }

    public void setCaAnnuelCommande(Float caAnnuelCommande) {
        this.caAnnuelCommande = caAnnuelCommande;
    }
}
        

        
    //<editor-fold defaultstate="collapsed" desc="GETTERS ET SETTERS">
    
