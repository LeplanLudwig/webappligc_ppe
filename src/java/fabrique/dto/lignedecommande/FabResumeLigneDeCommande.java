/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fabrique.dto.lignedecommande;

import dto.lignedecommande.ResumeLigneDeCommande;
import entites.LigneDeCommande;
import java.util.List;

/**
 *
 * @author remy917
 */
    public interface FabResumeLigneDeCommande {
    ResumeLigneDeCommande creerResumeLigneDeCommande(LigneDeCommande ldc);
    List<ResumeLigneDeCommande> creerListeResumeLigneDeCommande(List<LigneDeCommande> pListeLignesDeCommande);
}
