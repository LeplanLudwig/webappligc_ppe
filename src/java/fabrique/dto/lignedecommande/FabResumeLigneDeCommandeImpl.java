/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fabrique.dto.lignedecommande;

import dao.commande.DaoCommande;
import dto.lignedecommande.ResumeLigneDeCommande;
import entites.Commande;
import entites.LigneDeCommande;
import entites.Produit;
import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;
import javax.inject.Inject;
import javax.inject.Singleton;

@Singleton
public class FabResumeLigneDeCommandeImpl implements FabResumeLigneDeCommande, Serializable {
   
    @Inject DaoCommande daoCommande;

    @Override
    public ResumeLigneDeCommande creerResumeLigneDeCommande(LigneDeCommande ldc){
        
        ResumeLigneDeCommande rldc = new ResumeLigneDeCommande();
        Produit produit = ldc.getLeProduit();
        Commande cmd = ldc.getLaCommande();
        
        rldc.setRefprod(produit.getRefProd());
        rldc.setDesigProd(produit.getDesigProd());
        rldc.setQteCom(ldc.getQteCom());
        rldc.setPrixProd(produit.getPrixProd());
        rldc.setMontantTTC(ldc.getQteCom()*produit.getPrixProd());
        
        
        
        return rldc;
    }

    
    @Override
    public List<ResumeLigneDeCommande> creerListeResumeLigneDeCommande(List<LigneDeCommande> pListeLignesDeCommande){
        List<ResumeLigneDeCommande> listeRlgc = new LinkedList<ResumeLigneDeCommande>();
        for(LigneDeCommande lgc : pListeLignesDeCommande){
            listeRlgc.add(creerResumeLigneDeCommande(lgc));
        }
        return listeRlgc;
    }
    
}
