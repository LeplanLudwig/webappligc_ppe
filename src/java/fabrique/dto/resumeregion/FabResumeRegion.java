/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fabrique.dto.resumeregion;

import dto.region.ResumeRegion;
import entites.Region;
import java.util.List;

/**
 *
 * @author remy917
 */
public interface FabResumeRegion {
    ResumeRegion creerResumeRegion(Region region);
    ResumeRegion creerResumeRegion(String codeRegion);
    List<ResumeRegion> getTousLesResumeRegion(List<Region> listeRegion);
    List<ResumeRegion> getTousLesResumesRegion();
}
