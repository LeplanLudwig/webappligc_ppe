/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fabrique.dto.resumeregion;

import bal.region.BalRegion;
import dao.region.DaoRegion;
import dto.region.ResumeRegion;
import entites.Region;
import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;
import javax.inject.Inject;
import javax.inject.Singleton;

/**
 *
 * @author remy917
 */
@Singleton
public class FabResumeRegionImpl implements FabResumeRegion, Serializable{
    @Inject DaoRegion daoregion;
    @Inject BalRegion balregion;
    
    
    @Override
    public ResumeRegion creerResumeRegion(Region region){
        
        ResumeRegion rr = new ResumeRegion();
        
        rr.setCodeRegion(region.getCodeRegion());
        rr.setNomRegion(region.getNomRegion());
        rr.setCaAnnuel(balregion.caAnneeEnCours(region));
        rr.setNbClient(balregion.NbClient(region));
        
        return rr;
    }
    @Override
    public ResumeRegion creerResumeRegion(String codeRegion) {
        return creerResumeRegion(daoregion.getRegion(codeRegion));
    }
    @Override
    public List<ResumeRegion>getTousLesResumeRegion(List<Region> listeRegion){
        List<ResumeRegion> lrr = new LinkedList();
        
        for(Region r : listeRegion){
            lrr.add(this.creerResumeRegion(r));            
        }
        return lrr;
    }
    
    
    
    @Override
    public List<ResumeRegion> getTousLesResumesRegion(){
        return getTousLesResumeRegion(daoregion.getToutesLesRegions());
    }
    
}
