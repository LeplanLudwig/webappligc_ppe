
package fabrique.dto.commande;

import bal.commande.BalCommande;
import dao.client.DaoClient;
import dao.commande.DaoCommande;
import dto.commande.ResumeCommande;
import entites.Client;
import entites.Commande;
import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;
import javax.inject.Inject;
import javax.inject.Singleton;

    
    @Singleton
    public class FabResumeCommandeImpl implements FabResumeCommande, Serializable {
        @Inject DaoCommande daoCommande;
        @Inject DaoClient   daoClient;
        
        @Override
        public ResumeCommande creerResumeCommande(Commande commande){
            
            ResumeCommande rc = new ResumeCommande();
            rc.setNumcom(commande.getNumCom());
            rc.setDatecom(commande.getDateCom());
            rc.setNumcli(commande.getLeClient().getNumCli());
                rc.setNomcli(commande.getLeClient().getNomCli());
                rc.setAdrcli(commande.getLeClient().getAdrCli());
                rc.setCoderegion(commande.getLeClient().getLaRegion().getCodeRegion());
                rc.setNomregion(commande.getLeClient().getLaRegion().getNomRegion());
                rc.setEtatcom(commande.getEtatCom());
//                rc.setMontantHT(balCommande.montantCommandeHT(commande));
//                rc.setMontantTTC(balCommande.montantCommandeTTC(commande));
                
                
                return rc;
                
            }

    @Override
    public List<ResumeCommande> getResumeCommandeClient(Client client) {
        return getResumeCommande(daoCommande.getLesCommandesduClient(client));
    }
    @Override
    public List<ResumeCommande> getResumeCommandeClient(Long pnumclient){
        Client cli = daoClient.getLeClient(pnumclient);
        return getResumeCommandeClient(cli);
    }
    
    @Override
    public List<ResumeCommande> getResumeCommandeClient(Client client, int pAnnee) {
        return getResumeCommande(daoCommande.getLesCommandesduClient(client, pAnnee));
    }

    @Override
    public List<ResumeCommande> getResumeCommandeClient(Client client, int pAnnee, int pMois) {
        return getResumeCommande(daoCommande.getLesCommandesduClient(client, pAnnee, pMois));
    }

    @Override
    public ResumeCommande creerResumeCommande(Long numcom) {
        return creerResumeCommande(daoCommande.getCommande(numcom));
    }

    @Override
    public List<ResumeCommande> getResumeCommande(List<Commande> desCommandes) {
        List<ResumeCommande> lc=new LinkedList<ResumeCommande>();
        
        for(Commande c: desCommandes){ lc.add(creerResumeCommande(c));}
        return lc;
    }
        
        
        
        
    }
    

