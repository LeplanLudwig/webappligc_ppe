package fabrique.dto.commande;

import dto.commande.ResumeCommande;
import entites.Client;
import entites.Commande;
import java.util.List;

public interface FabResumeCommande{
    List<ResumeCommande> getResumeCommandeClient(Client client);
    List<ResumeCommande> getResumeCommandeClient(Client client, int pAnnee);
    List<ResumeCommande> getResumeCommandeClient(Client client, int pAnnee, int pMois);
    List<ResumeCommande> getResumeCommandeClient(Long pnumclient);
    
    ResumeCommande creerResumeCommande(Commande commande);
    ResumeCommande creerResumeCommande(Long numcom);
    List<ResumeCommande> getResumeCommande(List<Commande> desCommandes);
//    List<ResumeCommande> getResumeCommande(Long pclient);
}