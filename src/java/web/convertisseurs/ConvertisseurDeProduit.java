package web.convertisseurs;
import entites.Produit;
import javax.faces.convert.FacesConverter;
import web.beans.TableAssocProduits;
import web.helpers.JsfHelperBean;

@FacesConverter(forClass=Produit.class)
public class ConvertisseurDeProduit extends Convertisseur {
     
 @Override
 public String getAsString(Object objet){
   
     Produit    obj=(Produit)objet;
     return   cle(obj);
 }
 
 @Override
 public  Object getAsObject( String string) {
    
    TableAssocProduits ta=JsfHelperBean.getBean("#{tableAssocProduits}", TableAssocProduits.class);  
    return ta.getObjet(string);
 }
 
 private String cle(Object obj) {
     
        return ((Produit)obj).getRefProd();
 }

 
}



