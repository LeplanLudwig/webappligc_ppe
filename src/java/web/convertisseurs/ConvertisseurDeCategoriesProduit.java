package web.convertisseurs;
import entites.CategorieProduit;
import javax.faces.convert.FacesConverter;
import web.beans.TableAssocCategoriesProduit;
import web.helpers.JsfHelperBean;

@FacesConverter(forClass=CategorieProduit.class)
public class ConvertisseurDeCategoriesProduit extends Convertisseur {
     
 @Override
 public String getAsString(Object objet){
   
     CategorieProduit    obj=(CategorieProduit)objet;
     return   cle(obj);
 }
 
 @Override
 public  Object getAsObject( String string) {
    
    TableAssocCategoriesProduit ta=JsfHelperBean.getBean("#{tableAssocCategoriesProduit}", TableAssocCategoriesProduit.class);  
    return ta.getObjet(string);
 }
 
 private String cle(Object obj) {
     
        return ((CategorieProduit)obj).getCodeCateg().toString();
 }

 
}



