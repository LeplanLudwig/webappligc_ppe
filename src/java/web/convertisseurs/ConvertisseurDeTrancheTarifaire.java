package web.convertisseurs;
import entites.TrancheTarifaire;
import javax.faces.convert.FacesConverter;
import web.beans.TableAssocTrancheTarifaire;
import web.helpers.JsfHelperBean;

@FacesConverter(forClass=TrancheTarifaire.class)
public class ConvertisseurDeTrancheTarifaire extends Convertisseur {
     
 @Override
 public String getAsString(Object objet){
   
     TrancheTarifaire    obj=(TrancheTarifaire)objet;
     return   cle(obj);
 }
 
 @Override
 public  Object getAsObject( String string) {
    
    TableAssocTrancheTarifaire ta=JsfHelperBean.getBean("#{tableAssocTrancheTarifaire}", TableAssocTrancheTarifaire.class);  
    return ta.getObjet(string);
 }
 
 private String cle(Object obj) {
         return ((TrancheTarifaire)obj).getIdtranche().toString();
       // return Integer.toHexString(obj.hashCode());
 }

 
}



