//package web.beans;
//
//import dao.commande.DaoCommande;
//import entites.Commande;
//import java.io.Serializable;
//import java.util.LinkedHashMap;
//import java.util.List;
//import java.util.Map;
//import javax.annotation.PostConstruct;
//import javax.inject.Inject;
//import javax.inject.Named;
//import javax.inject.Singleton;
//
//
///**
// *
// * @author rsmon
// */
//
//
//@Singleton
//@Named
//public class TableAssocCommandes  implements Serializable {
//    
//      
//  //<editor-fold defaultstate="collapsed" desc="INJECTION DES DEPENDANCES">
//    
//    @Inject
//    private DaoCommande daoCommande;  
//    
//  //</editor-fold>
//
//  //<editor-fold  desc="DECLARATION DE LA LISTE ET CREATION DE LA TABLE ASSOCIATIVE">
//   
//    private List<Commande>                liste;
//    private Map<String, Commande>         table= new LinkedHashMap<String, Commande>();
//    
//    //</editor-fold>
//    
//  //<editor-fold  desc="INITIALISATION DE LA LISTE ET DE LE TABLE ASSOCIATIVE">
//    
//    @PostConstruct
//    public void init (){
//        
//        liste= daoCommande.getToutesLesCommandes();
//        
//        for(Commande obj: liste ){
//            
//            table.put(cle(obj), obj);        
//        }
//        
//        System.out.println("Initialisation Table Associative OK");
//    }
//    
//    //</editor-fold>
//   
//    public Commande getObjet(String cle){
// 
//        return table.get(cle);
//    } 
//    
//    public List<Commande> getListe() {
//        return liste;
//    }
//    
//    public void rafraichir(){
//       
//       table.clear();
//       init();
//    }
//    
//    //<editor-fold defaultstate="collapsed" desc="METHODES PRIVEES">
//   
//    private Long cle(Object obj) {
//        
//        return ((Commande)obj).getNumCom();
//        
//    }
//    
//    //</editor-fold>
//  
//}
//
//
