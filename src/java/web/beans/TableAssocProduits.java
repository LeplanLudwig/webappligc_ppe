package web.beans;

import dao.produit.DaoProduit;
import entites.Produit;
import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.inject.Named;
import javax.inject.Singleton;


/**
 *
 * @author rsmon
 */


@Singleton
@Named
public class TableAssocProduits implements Serializable {
    
      
  //<editor-fold defaultstate="collapsed" desc="INJECTION DES DEPENDANCES">
    
    @Inject
    private DaoProduit daoProduit;  
    
  //</editor-fold>

  //<editor-fold  desc="DECLARATION DE LA LISTE ET CREATION DE LA TABLE ASSOCIATIVE">
   
    private List<Produit>                liste;
    private Map<String, Produit>         table= new LinkedHashMap<String, Produit>();
    
    //</editor-fold>
    
  //<editor-fold  desc="INITIALISATION DE LA LISTE ET DE LE TABLE ASSOCIATIVE">
    
    @PostConstruct
    public void init (){
        
        liste= daoProduit.getTousLesProduits();
        
        for(Produit obj: liste ){
            
            table.put(cle(obj), obj);        
        }
        
        System.out.println("Initialisation Table Associative OK");
    }
    public void supprimer(Produit produit){
        table.remove(cle(produit));
        liste.remove(produit);
             
    }
    public void ajouter(Produit produit){
        table.put(cle(produit), produit);
        liste.add(produit);
    }
    
    //</editor-fold>
   
    public Produit getObjet(String cle){
 
        return table.get(cle);
    } 
    
    public List<Produit> getListe() {
        return liste;
    }
    
    public void rafraichir(){
       
       table.clear();
       init();
    }
    
    //<editor-fold defaultstate="collapsed" desc="METHODES PRIVEES">
   
    private String cle(Object obj) {
        
        return ((Produit)obj).getRefProd();     
    }
    
    //</editor-fold>
  
}


