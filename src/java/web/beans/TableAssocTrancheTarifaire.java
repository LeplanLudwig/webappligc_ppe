package web.beans;

import dao.tranchetarifaire.DaoTrancheTarifaire;
import entites.TrancheTarifaire;
import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.inject.Named;
import javax.inject.Singleton;


/**
 *
 * @author rsmon
 */


@Singleton
@Named
public class TableAssocTrancheTarifaire  implements Serializable {
    
      
  //<editor-fold defaultstate="collapsed" desc="INJECTION DES DEPENDANCES">
    
    @Inject
    private DaoTrancheTarifaire daoTrancheTarifaire;  
    
  //</editor-fold>

  //<editor-fold  desc="DECLARATION DE LA LISTE ET CREATION DE LA TABLE ASSOCIATIVE">
   
    private List<TrancheTarifaire>                liste;
    private Map<String, TrancheTarifaire>         table= new LinkedHashMap<String, TrancheTarifaire>();
    
    //</editor-fold>
    
  //<editor-fold  desc="INITIALISATION DE LA LISTE ET DE LE TABLE ASSOCIATIVE">
    
    @PostConstruct
    public void init (){
        
        liste= daoTrancheTarifaire.getTouteLesTranches();
        
        for(TrancheTarifaire obj: liste ){
            
            table.put(cle(obj), obj);        
        }
        
        System.out.println("Initialisation Table Associative OK");
    }
    public void supprimer(TrancheTarifaire tt){
        table.remove(cle(tt));
        liste.remove(tt);
             
    }
    public void ajouter(TrancheTarifaire tt){
        table.put(cle(tt), tt);
        liste.add(tt);
    }
    
    //</editor-fold>
   
    public TrancheTarifaire getObjet(String cle){
 
        return table.get(cle);
    } 
    
    public List<TrancheTarifaire> getListe() {
        return liste;
    }
    
    public void rafraichir(){
       
       table.clear();
       init();
    }
    
    //<editor-fold defaultstate="collapsed" desc="METHODES PRIVEES">
   
    private String cle(Object obj) {
        
        return ((TrancheTarifaire)obj).getIdtranche().toString();        
    }
    
    //</editor-fold>
  
}


