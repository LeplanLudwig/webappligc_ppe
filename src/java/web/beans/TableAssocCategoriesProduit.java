package web.beans;

import dao.categorieproduit.DaoCategorieProduit;
import entites.CategorieProduit;
import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.inject.Named;
import javax.inject.Singleton;


/**
 *
 * @author rsmon
 */


@Singleton
@Named
public class TableAssocCategoriesProduit  implements Serializable {
    
      
  //<editor-fold defaultstate="collapsed" desc="INJECTION DES DEPENDANCES">
    
    @Inject
    private DaoCategorieProduit daoCategorieProduit;  
    
  //</editor-fold>

  //<editor-fold  desc="DECLARATION DE LA LISTE ET CREATION DE LA TABLE ASSOCIATIVE">
   
    private List<CategorieProduit>                liste;
    private Map<String, CategorieProduit>         table= new LinkedHashMap<String, CategorieProduit>();
    
    //</editor-fold>
    
  //<editor-fold  desc="INITIALISATION DE LA LISTE ET DE LE TABLE ASSOCIATIVE">
    
    @PostConstruct
    public void init (){
        
        liste= daoCategorieProduit.getToutesLesCategories();
        
        for(CategorieProduit obj: liste ){
            
            table.put(cle(obj), obj);        
        }
        
        System.out.println("Initialisation Table Associative OK");
    }
    public void supprimer(CategorieProduit cp){
        table.remove(cle(cp));
        liste.remove(cp);
             
    }
    public void ajouter(CategorieProduit cp){
        table.put(cle(cp), cp);
        liste.add(cp);
    }
    
    //</editor-fold>
   
    public CategorieProduit getObjet(String cle){
 
        return table.get(cle);
    } 
    
    public List<CategorieProduit> getListe() {
        return liste;
    }
    
    public void rafraichir(){
       
       table.clear();
       init();
    }
    
    //<editor-fold defaultstate="collapsed" desc="METHODES PRIVEES">
   
    private String cle(Object obj) {
        
        return ((CategorieProduit)obj).getCodeCateg();        
    }
    
    //</editor-fold>
  
}


