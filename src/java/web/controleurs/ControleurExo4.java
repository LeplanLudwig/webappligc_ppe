
package web.controleurs;

import bal.client.BalClient;
import bal.commande.BalCommande;
import dao.Dao;
import entites.CategorieProduit;
import entites.Client;
import entites.Commande;
import entites.LigneDeCommande;
import entites.Produit;
import entites.Region;
import entites.TrancheTarifaire;
import java.io.IOException;
import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.enterprise.context.SessionScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ComponentSystemEvent;
import javax.inject.Inject;
import javax.inject.Named;
import org.primefaces.component.selectonemenu.SelectOneMenu;
import web.beans.TableAssocCategoriesProduit;
import web.beans.TableAssocClients;
import web.beans.TableAssocProduits;
import web.beans.TableAssocRegions;
import web.beans.TableAssocTrancheTarifaire;


@Named
@SessionScoped
public class ControleurExo4 implements Serializable{
     
    
    //<editor-fold defaultstate="collapsed" desc="Inject">
    @Inject Dao dao;
    
    @Inject
    private BalCommande balCommande;
    
    @Inject
    private BalClient balClient;
    
    @Inject
    private TableAssocClients tClients;
    @Inject
    private TableAssocProduits tProduits;
    
    @Inject
    private TableAssocRegions tRegions;
    
    @Inject
    private TableAssocTrancheTarifaire tTrancheTarifaire;
    
    @Inject
    private TableAssocCategoriesProduit tcp;
    
    @Inject
    private ControleurExo2 contExo2;
    //</editor-fold>
    
    //Déclaration de toutes les listes
    //<editor-fold defaultstate="collapsed" desc="Listes">
    private List<Client> lesClients;
    private List<Produit> lesProduits;
    private List<CategorieProduit> lesCategories;
    private List<TrancheTarifaire> lesTranches;
    private List<Region> lesRegions;
    private List<Commande> listeCommandes;
    //</editor-fold>
    
    // Déclaration de toutes les variables
    private Produit produit;
    private Client client;
    private TrancheTarifaire tranche;
    private CategorieProduit cp;
    private Region       region;
    private CategorieProduit categorie; 
    private Commande commandeSel;
    
    private Float caClient;
    private Float soldeClient;

    private String messageTableau;
    
    SelectOneMenu      choixRegion;
    
    // Déclaration des nouvelles variable pour la fonction ajouter dans le crud.
    //<editor-fold defaultstate="collapsed" desc="Nouvelles variables">
    private Client nouveauClient;
    private Produit nouveauProduit;
    private Region nouvelleRegion;
    private CategorieProduit nouvelleCategorie;
    //</editor-fold>
    
    public void prevalide(ComponentSystemEvent e){
         
         this.client=new Client();
        
         this.messageTableau=null;
    }
     
    
    @PostConstruct
    public void init(){
     
        tClients.rafraichir();
        lesClients=tClients.getListe();
        lesRegions=tRegions.getListe();
        lesProduits=tProduits.getListe();
        lesCategories = tcp.getListe();
        lesTranches = tTrancheTarifaire.getListe();
       
       
        // Si la variable est vide, il sélectionne le premier de la liste
        if (! lesClients.isEmpty()) {
           client=lesClients.get(0);   
           traiterSelectionCombo();
        }
        if(! lesProduits.isEmpty()){
            produit = lesProduits.get(0);
            traiterSelectionComboProduit();
        }
        if(! lesRegions.isEmpty()){
            region = lesRegions.get(0);
            
        }
        if(! lesCategories.isEmpty()){
            categorie = lesCategories.get(0);
        }
        if(! lesTranches.isEmpty()){
            tranche = lesTranches.get(0);
        }
        
    }
    
    
     public void traiterSelectionCombo(){
    
        if (client!=null){
           
            client.setLaRegion(tRegions.getObjet(client.getLaRegion().getCodeRegion()));
         
           messageTableau="Pas de commande pour ce client";
        } 
        
         System.out.println("Selection Combo");
        
    }
         public Float montantCommandeHt(Commande cmd){ return balCommande.montantCommandeHT(cmd);}
    
        public Float montantLigneCommandeHT(LigneDeCommande ldc) {
        
        Float montant=0F;

           montant+=ldc.getQteCom()*ldc.getLeProduit().getPrixProd();
        
        return montant;
    } 
        
         public void traiterSelectionComboProduit(){
    
        if (produit!=null){
           
            produit.setLaCategorie(tcp.getObjet(produit.getLaCategorie().getCodeCateg()));
         
           messageTableau="Pas de categorie pour ce produit";
        } 
        
         System.out.println("Selection Combo");
        
    
    } 
    
     
    public void traiterChangementRegion(){
    
        System.out.println("changement region");
      
       
    } 
     
     
    public String rafraichir(){
        this.tClients.rafraichir();return null;
    }
    
    public Float montantCommandeTTC(Commande cmd){
      return balCommande.montantCommandeTTC(cmd);
    }
    
    public Float calculCaClient(){

      return balClient.caAnneeEnCours(client);
    }
    
    public Float calculSoldeClient(){
      return -balClient.resteARegler(client);
    }
    
    public void afficherCommande() throws IOException{
      
//      FacesMessage msg = new FacesMessage("Commande Sélectionnée",commandeSel.getNumCom().toString());
//      FacesContext.getCurrentInstance().addMessage(null, msg);
        FacesContext.getCurrentInstance().getExternalContext().redirect("test.xhtml");
    }
 
    //<editor-fold defaultstate="collapsed" desc="Modification des Objets">
    public void enregisterModifications(){
        
        System.out.println("Modification enregistée");
        
        System.out.println(client.getNumCli());
        System.out.println(client.getNomCli());
        System.out.println(client.getAdrCli());
        System.out.println(client.getLaRegion());
        dao.repercuterMAJ(client);
        
    }
    public void enregisterProduit(){
        
        System.out.println("Modification enregistée");
        
        System.out.println(produit.getRefProd());
        System.out.println(produit.getDesigProd());
        System.out.println(produit.getPrixProd());
        System.out.println(produit.getLaCategorie());
        
        dao.repercuterMAJ(produit);
        
    }
    
    public void enregistrerRegion(){
        System.out.println("Modification enregistrée");
        System.out.println(region.getCodeRegion());
        System.out.println(region.getNomRegion());
        dao.repercuterMAJ(region);
    
    }
    public void enregistrerCategorie(){
        System.out.println("Modification enregistrée");
        System.out.println(categorie.getCodeCateg());
        System.out.println(categorie.getNomCateg());
        System.out.println(categorie.getTauxTVACateg());
        
        dao.repercuterMAJ(categorie);
    
    }
    public void enregistrerlaCategorie(){
        dao.enregistrerEntite(nouvelleCategorie);
        tcp.ajouter(nouvelleCategorie);
        
        int indexNouvPro=lesCategories.indexOf(nouvelleCategorie);
        setCategorie(lesCategories.get(indexNouvPro));
    }
    
    public void enregistrerlaRegion(){
        dao.enregistrerEntite(nouvelleRegion);
        tRegions.ajouter(nouvelleRegion);
        
        int indexNouvPro=lesRegions.indexOf(nouvelleRegion);
        setRegion(lesRegions.get(indexNouvPro));
    }
    public void enregistrerleproduit(){
        dao.enregistrerEntite(nouveauProduit);
        tProduits.ajouter(nouveauProduit);
        
        int indexNouvPro=lesProduits.indexOf(nouveauProduit);
        setProduit(lesProduits.get(indexNouvPro));
    }
    //</editor-fold>
    
    
    //<editor-fold defaultstate="collapsed" desc="Suppression des Objets">
    public void supprimerClient(){
        dao.supprimerEntite(client);
        this.tClients.supprimer(client);
        setClient(lesClients.get(0));
    }
    public void supprimerProduit(){
        dao.supprimerEntite(produit);
        this.tProduits.supprimer(produit);
        setProduit(lesProduits.get(0));
    }
    public void supprimerRegion(){
        dao.supprimerEntite(region);
        this.tRegions.supprimer(region);
        setRegion(lesRegions.get(0));
    }
    public void supprimerCategorie(){
        dao.supprimerEntite(categorie);
        this.tcp.supprimer(categorie);
        setCategorie(lesCategories.get(0));
    }
    //</editor-fold>
   

    //<editor-fold defaultstate="collapsed" desc="Ajouter un Objet">
    public void ajouterClient(){
        nouveauClient = new Client();
        nouveauClient.setNomCli("Saisir le nom");
        nouveauClient.setAdrCli("Saisir la ville");
        nouveauClient.setLaRegion(tRegions.getObjet("NPDC"));
        
        
        dao.enregistrerEntite(nouveauClient);
        
        tClients.ajouter(nouveauClient);
        
        int indexNouvCli=lesClients.indexOf(nouveauClient);
        setClient(lesClients.get(indexNouvCli));
    }
    
    public void ajouterProduit(){
        nouveauProduit = new Produit();
        nouveauProduit.setLaCategorie(tcp.getObjet("INF"));
        //        nouveauProduit.setRefProd("Saisir une référence");
        //        nouveauProduit.setDesigProd("Saisir la désignation");
        //        nouveauProduit.setPrixProd(0F);
        //        nouveauProduit.setLaCategorie(tcp.getObjet("INF"));
    }
    public void ajouterRegion(){
        nouvelleRegion = new Region();
    }
    public void ajouterCategorie(){
        nouvelleCategorie = new CategorieProduit();
    }
    //</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="GETTERS ET SETTERS">



    public Produit getNouveauProduit() {
        return nouveauProduit;
    }

    public void setNouveauProduit(Produit nouveauProduit) {
        this.nouveauProduit = nouveauProduit;
    }

    public Client getNouveauClient() {
        return nouveauClient;
    }

    public void setNouveauClient(Client nouveauClient) {
        this.nouveauClient = nouveauClient;
    }  
    public Client getClient() {
        return client;
    }
    
    public void setClient(Client client) {
        this.client = client;
    }
        public Produit getProduit() {
        return produit;
    }

    public void setProduit(Produit produit) {
        this.produit = produit;
    }
    
        public List<Produit> getLesProduits() {
        return lesProduits;
    }

    public void setLesProduits(List<Produit> lesProduits) {
        this.lesProduits = lesProduits;
    }
    
    public String getMessageTableau() {
        return messageTableau;
    }

    public void setMessageTableau(String messageTableau) {
        this.messageTableau = messageTableau;
    }
    
    public List<Client> getLesClients() {
        return lesClients;
    }

    public void setLesClients(List<Client> lesClients) {
        this.lesClients = lesClients;
    }

    public BalCommande getBalCommande() {
        return balCommande;
    }
    
    public BalClient getBalClient() {
        return balClient;
    }

    public Float getCaClient() {
        return caClient;
    }

    public Float getSoldeClient() {
        return soldeClient;
    }
    
     public Commande getCommandeSel() {
        return commandeSel;
    }

    public void setCommandeSel(Commande commandeSel) {
        this.commandeSel = commandeSel;
    }

    public List<TrancheTarifaire> getLesTranches() {
        return lesTranches;
    }

    public void setLesTranches(List<TrancheTarifaire> lesTranches) {
        this.lesTranches = lesTranches;
    }

    public TrancheTarifaire getTranche() {
        return tranche;
    }

    public void setTranche(TrancheTarifaire tranche) {
        this.tranche = tranche;
    }
        public List<Region> getLesRegions() {
        return lesRegions;
    }
   public List<CategorieProduit> getLesCategories() {
        return lesCategories;
    }
   public void setLesCategories(List<CategorieProduit> lesCategories) {
        this.lesCategories = lesCategories;
    }

    public void setLesRegions(List<Region> lesRegions) {
        this.lesRegions = lesRegions;
    }
    
        public List<Commande> getListeCommandes() {
        return listeCommandes;
    }
    
    public void setListeCommandes(List<Commande> listeCommandes) {
        this.listeCommandes = listeCommandes;
    }

    public Region getRegion() {
        return region;
    }

    public void setRegion(Region region) {
        this.region = region;
    }

    public Region getNouvelleRegion() {
        return nouvelleRegion;
    }

    public void setNouvelleRegion(Region nouvelleRegion) {
        this.nouvelleRegion = nouvelleRegion;
    }

    public CategorieProduit getCategorie() {
        return categorie;
    }

    public void setCategorie(CategorieProduit categorie) {
        this.categorie = categorie;
    }

    public CategorieProduit getNouvelleCategorie() {
        return nouvelleCategorie;
    }

    public void setNouvelleCategorie(CategorieProduit nouvelleCategorie) {
        this.nouvelleCategorie = nouvelleCategorie;
    }
    //</editor-fold>



    

    
    
}
