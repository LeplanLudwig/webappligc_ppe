/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package bal.lignedecommande;

import entites.LigneDeCommande;
import java.util.List;


/**
 *
 * @author remy917
 */
public interface BalLigneDeCommande {
    
    public Float caLigneDeCommandeHT (LigneDeCommande lignedecommande);
    public Float caLigneDeCommandeTTC (LigneDeCommande lignedecommande);
    
}
