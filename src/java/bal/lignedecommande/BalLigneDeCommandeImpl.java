
package bal.lignedecommande;

import bal.tranchetarifaire.BalTrancheTarifaire;
import dao.produit.DaoProduit;
import dao.tva.DaoTva;
import dto.lignedecommande.ResumeLigneDeCommande;
import entites.LigneDeCommande;
import java.io.Serializable;
import javax.inject.Inject;
import javax.inject.Singleton;

@Singleton
public class BalLigneDeCommandeImpl implements BalLigneDeCommande, Serializable {
    
    @Inject DaoTva daoTva;
    @Inject ResumeLigneDeCommande dto;
    @Inject BalTrancheTarifaire bal;
    @Inject DaoProduit daoProduit;
    Float tva;
    
@Override
public Float caLigneDeCommandeHT (LigneDeCommande lignedecommande){
    
    return lignedecommande.getQteCom()*bal.prixProd(lignedecommande.getLeProduit(), lignedecommande.getQteCom());
}

@Override
 public Float caLigneDeCommandeTTC (LigneDeCommande lignedecommande){

    return caLigneDeCommandeHT(lignedecommande)*(1+daoTva.getTauxTVA());

}
}
