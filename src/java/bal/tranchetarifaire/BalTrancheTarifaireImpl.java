
package bal.tranchetarifaire;

import dao.tranchetarifaire.DaoTrancheTarifaire;
import entites.Produit;
import entites.TrancheTarifaire;
import javax.inject.Inject;
import javax.inject.Singleton;

@Singleton
public class BalTrancheTarifaireImpl implements BalTrancheTarifaire {
    
    @Inject DaoTrancheTarifaire     daotranchetarifaire;
    
    @Override 
    public Float prixProd(Produit pProduit, Float qte){
        
        Float prixProd=0F;
        
        for(TrancheTarifaire tf : pProduit.getLesTranches())
            if((qte>=tf.getQteDebutTranche())&&(qte<=tf.getQteFinTranche())){
                prixProd=tf.getPrixUnitaireTranche();break;
            }
        return prixProd;
    }
    
}
