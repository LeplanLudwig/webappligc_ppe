
package bal.categorieproduit;

import bal.tranchetarifaire.BalTrancheTarifaire;
import dao.categorieproduit.DaoCategorieProduit;
import entites.CategorieProduit;
import entites.Commande;
import entites.LigneDeCommande;
import entites.Produit;
import java.io.Serializable;
import javax.inject.Inject;
import javax.inject.Singleton;



@Singleton
public class BalCategorieProduitImpl implements BalCategorieProduit, Serializable {
    
    
    @Inject DaoCategorieProduit DaoCategorieProduit;
    @Inject BalTrancheTarifaire    BalTT;
    
    
    @Override
    
    public Float caCategorieProduit (CategorieProduit pCategorieProduit){
        Float ca = 0F;
        for(Produit produit : pCategorieProduit.getLesProduits()){
            for (LigneDeCommande ldc : produit.getLesLignesDeCommande()){
                
                Commande cmd = ldc.getLaCommande();
                
            if(cmd.getEtatCom().equals("R")){
                ca+=BalTT.prixProd(produit, ldc.getQteCom());
            }
            }
        }
        return ca;
        
    }
    
    @Override
    public Float caCodeCategorieProduit (String cpcodeCateg){
        Float ca=0F;
        CategorieProduit categ = DaoCategorieProduit.getLaCategorie(cpcodeCateg);
        ca = caCategorieProduit(categ);
        
        return ca;
    }
}
