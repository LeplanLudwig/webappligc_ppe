package dao.orm;
import javax.inject.Inject;
import javax.inject.Singleton;
import javax.persistence.EntityManager;
import javax.persistence.Persistence;

/**
 *
 * @author rsmon
 */


@Singleton
public class DaoOrmImpl  {
   
    @Inject @Em EntityManager em;  
    
    public DaoOrmImpl() {
      
       em=Persistence.createEntityManagerFactory("PU").createEntityManager(); 
    }  
}


