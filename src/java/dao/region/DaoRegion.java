
package dao.region;

import entites.Region;
import java.util.List;

public interface DaoRegion {
    Region       getRegion(String codeReg);
    List<Region>  getToutesLesRegions();
}
