package dao.tranchetarifaire;

import entites.TrancheTarifaire;
import java.util.List;
import javax.inject.Singleton;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@Singleton
public class DaoTrancheTarifaireImpl implements DaoTrancheTarifaire {
    
    @PersistenceContext(unitName = "PU")
    private EntityManager em;
    
    @Override
    public List getLaTrancheTarifaire(int pIdTranche) {
        
        return em.createNamedQuery("Select tf from TrancheTarifaire tf").getResultList();
    }

    @Override
    public List<TrancheTarifaire> getTouteLesTranches() {
        return em.createQuery("Select tf from TrancheTarifaire tf").getResultList(); 
    }
}
