
package dao.tranchetarifaire;

import entites.TrancheTarifaire;
import java.util.List;


public interface DaoTrancheTarifaire {
    List<TrancheTarifaire>        getLaTrancheTarifaire(int pIdTranche);
    List<TrancheTarifaire>  getTouteLesTranches();
    
}
