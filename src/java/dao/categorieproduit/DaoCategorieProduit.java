
package dao.categorieproduit;
import entites.CategorieProduit;
import java.util.List;


public interface DaoCategorieProduit {
    
    CategorieProduit getLaCategorie(String CodeCateg);
    List<CategorieProduit> getToutesLesCategories();
    
}
