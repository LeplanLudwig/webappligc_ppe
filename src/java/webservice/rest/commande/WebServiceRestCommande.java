/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package webservice.rest.commande;

import dao.commande.DaoCommande;
import dto.commande.ResumeCommande;
import dto.lignedecommande.ResumeLigneDeCommande;
import entites.Commande;
import entites.LigneDeCommande;
import fabrique.dto.commande.FabResumeCommande;
import fabrique.dto.lignedecommande.FabResumeLigneDeCommande;
import java.util.List;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;

@Stateless
@Path("commande")
public class WebServiceRestCommande {
 
    
    //@Inject ModeleWebServCommande modele;
    @Inject FabResumeCommande fabcom;
    @Inject FabResumeLigneDeCommande fablignecom;
    @Inject DaoCommande daoCommande;
    
    
    @GET
    @Path("resumecommande/{numcom}")
    @Produces("application/xml")
    
    public ResumeCommande getResumeCommande(@PathParam("numcom")Long numcom){
        return fabcom.creerResumeCommande(numcom);        
    }
    @GET
    @Path("resumecommande/client/{numcli}")
    @Produces("application/xml")
    
    public java.util.List<ResumeCommande> getResumeCommandeClient(@PathParam("numcli")Long pnumclient){
        return fabcom.getResumeCommandeClient(pnumclient);     
    }
    @GET
    @Path("resumelignedecommande/{numcom}")
    @Produces({"application/xml", "application/json"})
    
    public List<ResumeLigneDeCommande> getLesLignesDeCommande(@PathParam("numcom")Long numcom){
        Commande cmd = daoCommande.getCommande(numcom);
        List<LigneDeCommande> ldc = cmd.getLesLignesDeCommande();
        List<ResumeLigneDeCommande> rldc = fablignecom.creerListeResumeLigneDeCommande(ldc);
        
        return rldc;
    }
//    
//    @GET 
//    @Path("hello")
//    @Produces("text/plain")
//    
//    public String hello(){
//        return "coucou";
//    }
}
