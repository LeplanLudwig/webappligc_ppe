/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package webservice.rest.region;


import dto.region.ResumeRegion;
import fabrique.dto.resumeregion.FabResumeRegion;
import java.util.List;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;

@Stateless
@Path("region")
public class WebServRestRegion {
    @Inject FabResumeRegion fabreg;
    
    @GET
    @Path("resumeregion/{codeRegion}")
    @Produces("application/xml")
    
    public ResumeRegion getResumeRegion(@PathParam("codeRegion")String codeRegion){
        return fabreg.creerResumeRegion(codeRegion);    
    }
    @GET
    @Path("listresumeregion/")
    @Produces("application/xml")
    
    public List<ResumeRegion> getListeResumeRegion(){
        return fabreg.getTousLesResumesRegion();
    }

//    @GET 
//    @Path("hello")
//    @Produces("text/plain")
//    
//    public String hello(){
//        return "coucou";
//    }
}
