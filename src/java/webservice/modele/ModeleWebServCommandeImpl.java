/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package webservice.modele;

import dto.commande.ResumeCommande;
import fabrique.dto.commande.FabResumeCommande;
import java.io.Serializable;
import javax.inject.Inject;

/**
 *
 * @author remy917
 */
public class ModeleWebServCommandeImpl implements Serializable, ModeleWebServCommande {
    
    @Inject FabResumeCommande fabResumeCommande;
    
    @Override
    public ResumeCommande creerResumeCommande(Long numcom){
        return fabResumeCommande.creerResumeCommande(numcom);
    }
}
