package entites;
import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.xml.bind.annotation.XmlRootElement;
import org.eclipse.persistence.annotations.PrivateOwned;

@Entity
@XmlRootElement
public class CategorieProduit implements Serializable {
   
    @Id
    private String codeCateg;
    private String nomCateg;
    private Float  tauxTVACateg;
    
    @PrivateOwned
    @OneToMany(mappedBy = "laCategorie", cascade=CascadeType.ALL)
    private List<Produit> lesProduits= new LinkedList<Produit>();
     
    public CategorieProduit() {}

    public CategorieProduit(String codeCateg, String nomCateg, Float tauxTVACateg) {
        this.codeCateg = codeCateg;
        this.nomCateg = nomCateg;
        this.tauxTVACateg = tauxTVACateg;
        
    }   
    
    public void afficher(){
    
        System.out.printf("%-8s %-20s",codeCateg,nomCateg,tauxTVACateg);
    }
    
    //<editor-fold defaultstate="collapsed" desc="GETTERS ET SETTERS">
    public String getCodeCateg() {
        return codeCateg;
    }
    public void setCodeCateg(String codeCateg) {
        this.codeCateg = codeCateg;
    }
    public String getNomCateg() {
        return nomCateg;
    }
    public void setNomCateg(String nomCateg) {
        this.nomCateg = nomCateg;
    }
    public List<Produit> getLesProduits() {
        return lesProduits;
    }
    public void setLesProduits(List<Produit> lesProduits) {
        this.lesProduits = lesProduits;
    }

    public Float getTauxTVACateg() {
        return tauxTVACateg;
    }

    public void setTauxTVACateg(Float tauxTVACateg) {
        this.tauxTVACateg = tauxTVACateg;
    }
 
    @Override
    public int hashCode() {
        int hash = 3;
        hash = 83 * hash + (this.codeCateg != null ? this.codeCateg.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final CategorieProduit other = (CategorieProduit) obj;
        if ((this.codeCateg == null) ? (other.codeCateg != null) : !this.codeCateg.equals(other.codeCateg)) {
            return false;
        }
        return true;
    }
}



