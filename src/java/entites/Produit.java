package entites;
import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.xml.bind.annotation.XmlRootElement;

@Entity
@XmlRootElement
public class Produit implements Serializable {
    
    @Id
    private String refProd;
   
    private String desigProd;
    private Float  prixProd;
    
    
   
    @OneToMany(mappedBy = "leProduit")
    private List<LigneDeCommande>lesLignesDeCommande=new LinkedList<LigneDeCommande>();
   
    @JoinColumn(name="CODECATEG")
    @ManyToOne
    private CategorieProduit laCategorie;
     
    
    @OneToMany(mappedBy = "leProduit")
    private List<TrancheTarifaire> lesTranches=new LinkedList<TrancheTarifaire>();
 
    
    public Produit (){ }

    public Produit(String refProd,String desigProd, Float prixProd, CategorieProduit categProd) {
       
        this.refProd   = refProd;
        this.desigProd = desigProd;
        this.prixProd  = prixProd;
        this.laCategorie= categProd;
    }
    
    public void afficher(){
    
    System.out.printf("%-10s  %-20s  %5.2f",refProd,desigProd,prixProd);
    }
    
    //<editor-fold defaultstate="collapsed" desc="GETTERS ET SETTERS">
    public String getRefProd() {
        return refProd;
    }
    public void   setRefProd(String refProd) {
        this.refProd = refProd;
    }
    public String getDesigProd() {
        return desigProd;
    }
    public void   setDesigProd(String desigProd) {
        this.desigProd = desigProd;
    }
    public List<LigneDeCommande> getLesLignesDeCommande() {
        return lesLignesDeCommande;
    }
    public void   setLesLignesDeCommande(List<LigneDeCommande> lesLignesDeCommande) {
        this.lesLignesDeCommande = lesLignesDeCommande;
    }
    public Float  getPrixProd() {
        return prixProd;
    }
    public void   setPrixProd(Float prixProd) {
        this.prixProd = prixProd;
    }
    
    //</editor-fold>

    public CategorieProduit getLaCategorie() {
        return laCategorie;
    }

    public void setLaCategorie(CategorieProduit laCategorie) {
        this.laCategorie = laCategorie;
    }

    public List<TrancheTarifaire> getLesTranches() {
        return lesTranches;
    }

    public void setLesTranches(List<TrancheTarifaire> lesTranches) {
        this.lesTranches = lesTranches;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 37 * hash + (this.refProd != null ? this.refProd.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Produit other = (Produit) obj;
        if ((this.refProd == null) ? (other.refProd != null) : !this.refProd.equals(other.refProd)) {
            return false;
        }
        return true;
    }
    
}
    
