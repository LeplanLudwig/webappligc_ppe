package entites;
import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.xml.bind.annotation.XmlRootElement;
import org.eclipse.persistence.annotations.PrivateOwned;

@Entity
@XmlRootElement
public class Client implements Serializable {
  
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long   numCli;
    private String nomCli;
    private String adrCli;
    private Float  tauxRemise;
    
    @JoinColumn(name="CODEREG")
    @ManyToOne
    private Region laRegion;
    
    @PrivateOwned
    @OneToMany(mappedBy = "leClient", cascade=CascadeType.ALL)
    private List<Commande> lesCommandes=new LinkedList<Commande>();

    public Client() {}

    public Client(Long numCli,String nomCli, String adrCli, Region laRegion, Float tauxRemise) {
        
        this.numCli = numCli;
        this.nomCli = nomCli;
        this.adrCli = adrCli;
        this.tauxRemise = tauxRemise;
      
        this.laRegion = laRegion;
    }

    @Override
    public String toString() {return nomCli ;}
    
    public void afficher(){System.out.printf("%4d %-20s %-60s",numCli,nomCli,adrCli,tauxRemise);}
    
    //<editor-fold defaultstate="collapsed" desc="GETTERS ET SETTERS">
    
    public Long getNumCli() {
        return numCli;
    }
    public void setNumCli(Long numCli) {
        this.numCli = numCli;
    }
    
    public String getNomCli() {
        return nomCli;
    }
    public void setNomCli(String nomCli) {
        this.nomCli = nomCli;
    }
    
    public String getAdrCli() {
        return adrCli;
    }
    public void setAdrCli(String adrCli) {
        this.adrCli = adrCli;
    }
    
    
    
    public Region getLaRegion() {
        return laRegion;
    }
    
    public void setLaRegion(Region laRegion) {
        this.laRegion = laRegion;
    }
    
    public List<Commande> getLesCommandes() {
        return lesCommandes;
    }
    
    public void setLesCommandes(List<Commande> lesCommandes) {
        this.lesCommandes = lesCommandes;
    }

    public Float getTauxRemise() {
        return tauxRemise;
    }

    public void setTauxRemise(Float tauxRemise) {
        this.tauxRemise = tauxRemise;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 71 * hash + (this.numCli != null ? this.numCli.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Client other = (Client) obj;
        if (this.numCli != other.numCli && (this.numCli == null || !this.numCli.equals(other.numCli))) {
            return false;
        }
        return true;
    }
    
}



