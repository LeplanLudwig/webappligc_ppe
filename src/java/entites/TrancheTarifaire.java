package entites;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.xml.bind.annotation.XmlRootElement;

@Entity
@XmlRootElement
public class TrancheTarifaire implements Serializable {
    
    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    private Long idtranche;
    private Long qteDebutTranche;
    private Float qteFinTranche;
    private Float prixUnitaireTranche;
   
    
    @ManyToOne
    private Produit leProduit;
    
   
    public TrancheTarifaire(){}

    public TrancheTarifaire(Long idtranche, Long qteDebutTranche, Float qteFinTranche, Float prixUnitaireTranche, Produit leProduit) {
        this.idtranche = idtranche;
        this.qteDebutTranche = qteDebutTranche;
        this.qteFinTranche = qteFinTranche;
        this.prixUnitaireTranche = prixUnitaireTranche;
        this.leProduit = leProduit;
    }

   
    public Long getIdtranche() {
        return idtranche;
    }

    public void setIdtranche(Long idtranche) {
        this.idtranche = idtranche;
    }

    public Long getQteDebutTranche() {
        return qteDebutTranche;
    }

    public void setQteDebutTranche(Long qteDebutTranche) {
        this.qteDebutTranche = qteDebutTranche;
    }

    public Float getQteFinTranche() {
        return qteFinTranche;
    }

    public void setQteFinTranche(Float qteFinTranche) {
        this.qteFinTranche = qteFinTranche;
    }

    public Float getPrixUnitaireTranche() {
        return prixUnitaireTranche;
    }

    public void setPrixUnitaireTranche(Float prixUnitaireTranche) {
        this.prixUnitaireTranche = prixUnitaireTranche;
    }

    public Produit getLeProduit() {
        return leProduit;
    }

    public void setLeProduit(Produit leProduit) {
        this.leProduit = leProduit;
    }

   
 
}